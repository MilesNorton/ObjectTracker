#ifndef __DFTTRACKER_H__
#define __DFTTRACKER_H__


#include <include\Tracker.h>
#include "include\DistributionField.h"

class DftTracker : public Tracker
{
    public:
        DftTracker();
        ~DftTracker();

        //Need to use this instead to
        virtual Rectangle track(vil_image_view<double> inputImages, int i);

    protected:
    private:
};

#endif // DFTTRACKER_H
