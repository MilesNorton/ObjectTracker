#ifndef IMAGEHANDLER_H
#define IMAGEHANDLER_H
#include <vcl_vector.h>
#include <vil/vil_image_view.h>
#include "include\InputArguments.h"
#include <vcl_string.h>
#include <string>


using namespace std;
class ImageHandler
{
    public:
        ImageHandler();
        ~ImageHandler();
        //vcl_vector< vil_image_view<unsigned char> > getImages(vcl_string inputArgumentsDirectory,vcl_string  inputArgumentsExtension);

        int findImagesOnDisk(string inputArgsDir, string inputArgsExtension);

        vil_image_view<unsigned char> getImage(int number);

        vil_image_view<double> convertToDouble(vil_image_view<unsigned char> imageView);

        vil_image_view<double> convertColour(vil_image_view<double> imageView, string colourChoice);

        void saveImages(vil_image_view<unsigned char>  outputImages, vcl_string outputArgumentsDirectory, vcl_string inputArgumentsExtension, int outputValue);\
    protected:
    private:
        vector<string> imageFiles;
};

#endif // IMAGEHANDLER_H
