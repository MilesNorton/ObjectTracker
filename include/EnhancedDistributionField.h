#ifndef ENHANCEDDISTRIBUTIONFIELD_H
#define ENHANCEDDISTRIBUTIONFIELD_H

#include <vcl_vector.h>
#include <vil/vil_image_view.h>

class EnhancedDistributionField
{
    public:
        EnhancedDistributionField();
        ~EnhancedDistributionField();

        vcl_vector<vil_image_view<double> > ChannelRepresentation(vil_image_view<double>  inputImage,double greyBlur, double greyBlurSize, int Num_channels);

    protected:
    private:
};

#endif // ENHANCEDDISTRIBUTIONFIELD_H
