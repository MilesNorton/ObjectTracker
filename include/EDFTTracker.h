#ifndef EDFTTRACKER_H
#define EDFTTRACKER_H
#include <include\Tracker.h>
#include "include\InputArguments.h"
#include "include\DistributionField.h"
#include "include\Motion.h"

class EDFTTracker : public Tracker
{
    public:
        EDFTTracker();
        ~EDFTTracker();

        //Need to use this function instead for polymorphism
        virtual Rectangle track(vil_image_view<double> inputImages, int i);

    protected:
        Motion expectedMotion;
        //returns the expected location so that the object will be guaranteed to be in the
        //image in its entirety
        int boundsCheck(int expectedLocation, int objectDimension, int imageDimension);
    private:
};

#endif // EDFTTRACKER_H
