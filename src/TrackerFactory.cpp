#include "include\TrackerFactory.h"

TrackerFactory::TrackerFactory()
{
    //ctor
}

TrackerFactory::~TrackerFactory()
{
    //dtor
}


Tracker* TrackerFactory::createTracker(InputArguments inputArguments){

    //create a point of type tracker
    Tracker* tracker;

    //Create a EDFT tracker if algorithm is set to EDFT, else a DFT tracker
    if(inputArguments.getAlgorithmType() == "EDFT")
    {
        tracker = new EDFTTracker;
    }
    else
    {
        tracker = new DftTracker;
    }

    //Initialise the tracker with the input arguments
    tracker->initialise(inputArguments);

    return tracker;
}
