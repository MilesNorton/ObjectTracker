#ifndef DRAWRECTANGLE_H
#define DRAWRECTANGLE_H
#include <vcl_vector.h>
#include <vil/vil_image_view.h>
#include "include\Rectangle.h"
class DrawRectangle
{
    public:
        DrawRectangle();
        ~DrawRectangle();
        vil_image_view<unsigned char> drawFrame(vil_image_view<unsigned char> image, Rectangle rectangle);
    protected:
    private:
};

#endif // DRAWRECTANGLE_H
