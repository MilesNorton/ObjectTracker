#include "include\DrawRectangle.h"
#include "include\Rectangle.h"
#include <vil/vil_image_view.h>

DrawRectangle::DrawRectangle()
{

}

DrawRectangle::~DrawRectangle()
{

}

vil_image_view<unsigned char> DrawRectangle::drawFrame(vil_image_view<unsigned char> image, Rectangle rectangle){//must be colour


double finalX = rectangle.getEndPositionX();   //get ending y position(bottom right) of the rectangle box
double finalY = rectangle.getEndPositionY();   //get ending x position(bottom right) of the rectangle box
double initialX = rectangle.getX();            //get starting x position(top left) of the rectangle box
double initialY = rectangle.getY();            //get starting y position(top left) of the rectangle box

	vil_image_view<unsigned char>output(image.ni(), image.nj(), 3); // can create size and nplanes outside of functions

	//the below loop adds the values form image to the output the only difference is that it is as 3 plane output
	// gray scale in a 3 plane is equivalent to =>  grayscaleValue[i] =  red[i] = blue[i] = green[i]
    for (int x = 0; x < output.ni();x++){
        for (int y = 0; y < output.nj();y++){
            output(x,y,0) = image(x,y,0);
             output(x,y,1) = image(x,y,1);
            output(x,y,2) = image(x,y,2);
        }
    }

    //set colour of the box outline to green => Red = 0; Green = 255; Blue = 0
    for (double x = initialX; x < finalX; x++){
        //draw the top horizontal line
        output( x, initialY, 0) = 0;
        output( x, initialY, 1) = 255;   //green pixel
        output( x, initialY, 2) = 0;
        //draw the bottom horizontal line
        output( x, finalY, 0) = 0;
        output( x, finalY, 1) = 255;     //green pixel
        output( x, finalY, 2) = 0;
    }

    for(double y = initialY; y < finalY; y++){    //loop through the vertical line of the squared shape
        //draw the left vertival line
        output( initialX, y, 0) = 0;
        output( initialX, y, 1) = 255;   //green pixel
        output( initialX, y, 2) = 0;
        //draw the right vertical line
        output( finalX, y, 0) = 0;
        output( finalX, y, 1) = 255;     //green pixel
        output( finalX, y, 2) = 0;
    }
    return output;
}



