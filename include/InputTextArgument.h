#ifndef INPUTTEXTARGUMENT_H
#define INPUTTEXTARGUMENT_H

#include <string>
#include <sstream>
#include <istream>
#include <stdlib.h>

class InputTextArgument
{
    public:
        InputTextArgument();
        InputTextArgument(std::string data) : argument(data){}
        virtual ~InputTextArgument();

        //Methods to return the value as a string, unsigned int or double
        //Makes it much more convenient to get the right datatype
        std::string getAsString();
        unsigned int getAsUInt();
        double getAsDouble();
    protected:
    private:
        std::string argument;
};

#endif // INPUTTEXTARGUMENT_H
