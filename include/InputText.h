#ifndef INPUTTEXT_H
#define INPUTTEXT_H

#include "InputTextArgument.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <map>
#include <vector>
#include <sstream>
#include <iterator>

using namespace std;

class InputText
{
    public:
        InputText();
        virtual ~InputText();
        std::map<string, InputTextArgument> getText();
//        string* GetText();
    protected:
    private:
};

#endif // INPUTTEXT_H
