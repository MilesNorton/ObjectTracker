#include "include\InputArgumentHandler.h"
#include "include\InputText.h"
#include <vul/vul_arg.h>

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vil/vil_image_view.h>



#include <iomanip>
using namespace std;
InputArgumentHandler::InputArgumentHandler()
{

}

InputArgumentHandler::~InputArgumentHandler()
{

}

InputArguments InputArgumentHandler::getArguments(int argc, char* argv[]){

    //Create the output object
    InputArguments setArguments;

    InputText textTest;

    std::map<string, InputTextArgument> inputArgsMap = textTest.getText();

    string algorithmDefault = compareWithDefault(inputArgsMap["ALGORITHM"].getAsString(), "DFT");
    string colourInputDefault = compareWithDefault(inputArgsMap["COLOURINPUT"].getAsString(), "COLOUR");

	vul_arg<vcl_string>
		argInputPath("-path", "Input path, i.e. C:/somefiles/", inputArgsMap["PATH"].getAsString()),                // path to look for input images
		argOutputPath("-outputPath","Output path", inputArgsMap["OUTPUTPATH"].getAsString()),                             // where to store the output images
		argAlgorithmType("-algorithm","choose: DFT or EDFT", algorithmDefault),            // user chooses DFT or EDFT algorithm double check this if we can have a default string
		argGlob("-glob", "Input glob, i.e. *png, this will get all png's.", inputArgsMap["GLOB"].getAsString()),    //file extension
		argColourInput("-colourInput", "Colour or Greyscale Input Type", inputArgsMap["COLOURINPUT"].getAsString());

	// input arguments for the algorithm

	unsigned int maxDistanceDefault = compareWithDefault(inputArgsMap["MAXDISTANCE"].getAsUInt(), 30);
	unsigned int channelSizeDefault = compareWithDefault(inputArgsMap["CHANNELSIZE"].getAsUInt(), 6);

	vul_arg<unsigned>
        argMaxSearchDistance("-maxDistance", "Max distance in DFT algorithm", maxDistanceDefault),  // Maximum distance used in DFT function
        argChannelSize("-channelSize","channel size of images", channelSizeDefault);


    double colourBlurDefault        = compareWithDefault(inputArgsMap["COLOURBLUR"].getAsDouble(), 0.65);
    double greyBlurDefault          = compareWithDefault(inputArgsMap["GREYBLUR"].getAsDouble(), 1);
    double colourBlurSizeDefault    = compareWithDefault(inputArgsMap["COLOURBLURSIZE"].getAsDouble(), 1);
    double greyBlurSizeDefault      = compareWithDefault(inputArgsMap["GREYBLURSIZE"].getAsDouble(), 4);
    double learningFactorDefault           = compareWithDefault(inputArgsMap["LEARNINGFACTOR"].getAsDouble(),0.05);

    vul_arg<double>
        argWidth("-objectWidth","width of the object", inputArgsMap["OBJECTWIDTH"].getAsDouble()),
        argHeight("-objectHeight","height of the object", inputArgsMap["OBJECTHEIGHT"].getAsDouble()),
        argInitialX("-objectX", "x location of the object", inputArgsMap["OBJECTX"].getAsDouble()),                        // location of the object in x plane
		argInitialY("-objectY", "y location of the object", inputArgsMap["OBJECTY"].getAsDouble()),                        // location of the object in y plane
        argColourBlur("-colourBlur", "", colourBlurDefault),           //colour blur for 1d gauss filter
        argGreyBlur("-GreyBlur", "", greyBlurDefault),    //GREYSCALE blur for 2d gauss filter
        argColourBlurSize("-colourBlurSize", "",colourBlurSizeDefault),           //colour blur for 1d gauss filter
        argGreyBlurSize("-GreyBlurSize", "", greyBlurSizeDefault),  //GREYSCALE blur for 2d gauss filter
        argLearningFactor("-LearningFactor", "What learning factor would you apply in the DF algorithms",learningFactorDefault);


	// call vul_arg_parse(argc, argv) to parse the arguments, this will go through the command line string, look for all the specified argument string,
	// and extract the provided values.
	vul_arg_parse(argc, argv);



//need to test..

    if (((argInputPath() == "") || (argGlob() == "") || (argOutputPath() == "")))// || (argInitialX() == "")|| (argInitialY() == "")|| (argWidth() == "")|| (argHeight() == ""))
	{
		// if we need these arguments to proceed, we can now exit and print the help as we quit. vul_arg_display_usage_and_exit() will display the available arguments
		// alongside the help message specified when the vul_arg objects were created
		vul_arg_display_usage_and_exit();
	}

	//set all the arguments in InputArguments Class8
    setArguments.setInitialX(argInitialX());
    setArguments.setInitialY(argInitialY());
    setArguments.setMaxSearchDistance(argMaxSearchDistance());
    setArguments.setInputPath(argInputPath());
    setArguments.setOutputPath(argOutputPath());
    setArguments.setAlgorithmType(argAlgorithmType());
    setArguments.setFileExtension(argGlob());
    setArguments.setcolourBlur(argColourBlur());
    setArguments.setgreyBlur(argGreyBlur());
    setArguments.setInitialWidth(argWidth());
    setArguments.setInitialHeight(argHeight());
    setArguments.setChannelSize(argChannelSize());
    setArguments.setColourBlurSize(argColourBlurSize());           //colour blur for 1d gauss filter
    setArguments.setGreyBlurSize(argGreyBlurSize());  //GREYSCALE blur for 2d gauss filter
    setArguments.setColourInput(argColourInput());
    setArguments.setColourInput(argColourInput());
    setArguments.setLearningFactor(argLearningFactor());

    inputArgsMap.clear();

    return setArguments;
}

double InputArgumentHandler::compareWithDefault(double number, double defaultVal){
    if(number == 0){
        return defaultVal;
    } else {
        return number;
    }
}
unsigned int InputArgumentHandler::compareWithDefault(unsigned int number, unsigned int defaultVal){
    if(number == 0){
        return defaultVal;
    } else {
        return number;
    }
}
string InputArgumentHandler::compareWithDefault(string str, string defaultVal){
    if(str == ""){
        return defaultVal;
    } else {
        return str;
    }
}
