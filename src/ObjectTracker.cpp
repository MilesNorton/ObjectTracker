
#include "include\Tracker.h"
#include "include\ImageHandler.h"
#include "include\TrackerFactory.h"
#include "include\InputArgumentHandler.h"
#include "include\DftTracker.h"
#include <vil/vil_image_view.h>
#include <vcl_vector.h>
#include "include\DrawRectangle.h"
#include "include\Rectangle.h"

#include <iomanip>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

using namespace std;
int main (int argc, char * argv[])
{

    // grab the user input values
    // A command line input will overwrite all text file inputs
    InputArgumentHandler inputArgumentHandler;
    InputArguments inputArguments = inputArgumentHandler.getArguments( argc, argv);   // pass in CMD arguments

    // Load in the images from the directory specified by the user
    ImageHandler imageHandler;
    int numberOfImages = imageHandler.findImagesOnDisk(inputArguments.getInputPath(), inputArguments.getFileExtension());

    //Initialise the tracker class which holds all user inputs used for the DFT/EDFT classes
    TrackerFactory trackerFactory;
    Tracker* tracker = trackerFactory.createTracker(inputArguments);
    tracker->initialise(inputArguments);

    // construct a drawrectangle instance
    DrawRectangle drawRectangle;

    vcl_ofstream txtFile;
    txtFile.open("outputcomparison.txt");

    for (int i = 0; i < numberOfImages;i++){

        //load the images
        vil_image_view<unsigned char> image = imageHandler.getImage(i);

        // converts the users image to type double
        vil_image_view<double> trackedImageBuffer = imageHandler.convertToDouble(image);

        //this handles converting the users image to greyscale or to colour
        vil_image_view<double> trackedImage = imageHandler.convertColour(trackedImageBuffer, inputArguments.getColourInput());

        // start the tracking algorithm
        Rectangle targetPositions = tracker->track(trackedImage, i);

        // print object values to the txt file
        txtFile  << vcl_fixed << vcl_setprecision(2) << targetPositions.getX() << " "
                << vcl_fixed << vcl_setprecision(2) << targetPositions.getY() << " "
                << vcl_fixed << vcl_setprecision(2) << targetPositions.getWidth() << " "
                << vcl_fixed << vcl_setprecision(2) << targetPositions.getHeight()<< " "<<vcl_endl;

        // draw rectangles on the images
        vil_image_view<unsigned char> outputImage = drawRectangle.drawFrame(image, targetPositions);

        // save the images
        imageHandler.saveImages(outputImage, inputArguments.getOutputPath(), inputArguments.getFileExtension(), i + 1);
    }
    txtFile.close();
}
