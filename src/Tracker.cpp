#include "include\Tracker.h"
#include "include\Rectangle.h"
#include <vil/vil_image_view.h>
#include "include\DftTracker.h"


#include <vil/vil_save.h>

Tracker::Tracker()
{
    //ctor
}

Tracker::~Tracker()
{
    //dtor
}

void Tracker::initialise(InputArguments inputArguments){

    //initialise all user inputs for the main algoirthm
    greyBlur = inputArguments.getGreyBlur();

    greyBlurSize = inputArguments.getGreyBlurSize();

    colourBlur = inputArguments.getColourBlur();

    colourBlurSize = inputArguments.getColourBlurSize();

    channelSize = inputArguments.getChannelSize();

    maxDistance = inputArguments.getMaxSearchDistance();

    learningFactor = inputArguments.getLearningFactor();

    Rectangle rectangle(inputArguments.getInitialX(), inputArguments.getInitialY(),
                        inputArguments.getInitialWidth(), inputArguments.getInitialHeight());

    initialObject = rectangle;

    currentObject = initialObject;
}
