#ifndef TRACKERFACTORY_H
#define TRACKERFACTORY_H

#include "include\Tracker.h"
#include "include\DftTracker.h"
#include "include\EDFTTracker.h"
#include "include\InputArguments.h"

class TrackerFactory
{
    public:
        TrackerFactory();
        ~TrackerFactory();

        // selects whether or not we need use EDFT/DFT and initialises the arguments
        Tracker* createTracker(InputArguments inputArguments);

    protected:
    private:
};

#endif // TRACKERFACTORY_H
