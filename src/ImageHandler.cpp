#include "include\ImageHandler.h"
#include <vcl_string.h>
#include <vcl_iostream.h>
#include <vcl_vector.h>
#include <vil/vil_image_view.h>
#include <vul/vul_file_iterator.h>
#include <vul/vul_file.h>
#include <vil/vil_load.h>
#include <vcl_string.h>
#include <vil/vil_save.h>

ImageHandler::ImageHandler()
{

}

ImageHandler::~ImageHandler()
{

}

int ImageHandler::findImagesOnDisk(string inputArgsDir, string inputArgsExtension){
    //fill the imageNames vector with each images location
    for (vul_file_iterator imageNameStrings=( inputArgsDir + "/*" + inputArgsExtension); imageNameStrings; ++imageNameStrings){
		if (!vul_file::is_directory(imageNameStrings())){
			imageFiles.push_back(imageNameStrings());
		}
	}

	return imageFiles.size();
}

vil_image_view<unsigned char> ImageHandler::getImage(int number){
    return vil_load(imageFiles[number].c_str());
}


vil_image_view<double> ImageHandler::convertToDouble(vil_image_view<unsigned char> image){
    vil_image_view<double> outputImage(image.ni(), image.nj(), image.nplanes());

    for(int x = 0; x < image.ni(); x++){
        for(int y = 0; y < image.nj(); y++){
            for (int c = 0; c < image.nplanes(); c++){
                outputImage( x, y, c) = image(x,y,c);
            }
        }
    }
    return outputImage;
}

vil_image_view<double> ImageHandler::convertColour(vil_image_view<double> image, string colourChoice){
    vil_image_view<double> outputImage(image.ni(), image.nj(), image.nplanes());


    if( colourChoice == "GREY" ){
        for(int x = 0; x < image.ni(); x++){
            for(int y = 0; y < image.nj(); y++){
                for (int c = 0; c < image.nplanes(); c++){
                         outputImage( x, y, c) = (image(x,y,0)+image(x,y,1)+image(x,y,2))/3; //greyscale
                }
            }
        }
    }
    else{
        for(int x = 0; x < image.ni(); x++){
            for(int y = 0; y < image.nj(); y++){
                for (int c = 0; c < image.nplanes(); c++){
                    outputImage( x, y, c) = image(x,y,c);   //colour
                }
            }
        }
    }
    return outputImage;
}


void ImageHandler::saveImages( vil_image_view<unsigned char>  outputImages, vcl_string outputArgumentsDirectory , vcl_string inputArgumentsExtension, int outputValue){

    vcl_stringstream outputName;

    outputName << outputArgumentsDirectory << "output" << outputValue << inputArgumentsExtension.substr(1);   // new output name with directory

    vil_save(outputImages,outputName.str().c_str());  //save new image

    outputName.str(""); //clear string

    outputName.clear(); //clear string
}

