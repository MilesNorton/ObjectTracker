#ifndef INPUTARGUMENTS_H
#define INPUTARGUMENTS_H
#include <vcl_string.h>

class InputArguments
{
    public:
        InputArguments();
        ~InputArguments();


            // add getter and setter functions
            double getInitialX() { return initialX; }
            double getInitialY() { return initialY; }
            double getInitialWidth() { return initialWidth; }
            double getInitialHeight() { return initialHeight; }
            double getLearningFactor() { return learningFactor; }
            vcl_string getColourInput() { return colourInput; }
            unsigned int getMaxSearchDistance(){return maxSearchDistance;}
            double getColourBlur() { return colourBlur; }
            double getGreyBlur() { return greyBlur; }
            double getColourBlurSize(){ return greyBlurSize;}           //colour blur for 1d gauss filter
            double getGreyBlurSize(){return colourBlurSize;}  //GREYSCALE blur for 2d gauss filter
            vcl_string getAlgorithmType(){return algorithmType;}
            vcl_string getInputPath() { return inputPath; }
            vcl_string getOutputPath() { return outputPath; }
            vcl_string getFileExtension(){return fileExtension;}
            int getChannelSize(){return channelSize;}




            void setInitialX(double startingX);
            void setInitialY(double startingY);
            void setInitialWidth(double startingWidth);
            void setInitialHeight(double startingHeight);
            void setMaxSearchDistance(unsigned int searchDistance);
            void setcolourBlur(double CBlur);
            void setgreyBlur(double GBlur);
            void setAlgorithmType(vcl_string algorithm);
            void setInputPath(vcl_string input);
            void setOutputPath(vcl_string output);
            void setFileExtension(vcl_string extension);
            void setChannelSize(int ChannelSize);
            void setLearningFactor(double LearningFactor);
            void setColourBlurSize( double GreyBlurSize);           //colour blur for 1d gauss filter
            void setGreyBlurSize( double ColourBlurSize);  //GREYSCALE blur for 2d gauss filter
            void setColourInput(vcl_string ColourInput);

        protected:
        private:
            double greyBlur;
            double colourBlur;
            double greyBlurSize;
            double colourBlurSize;
            double initialX;
            double initialY;
            double initialWidth;
            double initialHeight;
            double channelSize;
            double learningFactor;
            unsigned int maxSearchDistance;
            vcl_string algorithmType;
            vcl_string inputPath;
            vcl_string outputPath;
            vcl_string fileExtension;
            vcl_string colourInput;

};

#endif // INPUTARGUMENTS_H
