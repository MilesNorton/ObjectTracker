#include "include\EDFTTracker.h"
#include "include\GradientDescent.h"
#include "include\Rectangle.h"
#include "include\EnhancedDistributionField.h"
#include <vil/vil_crop.h>

EDFTTracker::EDFTTracker()
{
    //ctor
}

EDFTTracker::~EDFTTracker()
{
    //dtor
}


Rectangle EDFTTracker::track(vil_image_view<double> inputImage, int i){

    //Store the position of the object before this frame
    Rectangle initialPosition = currentObject;

    //initialise The Distrbuition Field Algorithm
    DistributionField DfAlgorithm;

    //initialise The Enhanced Distrbuition Field Algorithm
    EnhancedDistributionField EDFTAlgorithm;

    // initialise the greadient algorithm
    GradientDescent gradientDescent;

    // intialise rectangle
    Rectangle rectangle;

    // create a vector to store the objects lengths
    vcl_vector<double> objectPosition;

    //store the object values in this vector

    //initialise model and object model images here
    vcl_vector<vil_image_view<double> > channelRepresentationInput;
    vil_image_view <double> objectModelUpdate;
    vcl_vector<vil_image_view<double> > objectUpdate;

    // create a channel representation of the image
    channelRepresentationInput = EDFTAlgorithm.ChannelRepresentation(inputImage, greyBlur, greyBlurSize, channelSize);

    if (i == 0)//first frame
    {
        //create a channel representation of the model
        objectModelUpdate = vil_crop(inputImage, currentObject.getX(), currentObject.getWidth(), currentObject.getY(), currentObject.getHeight());
        model = EDFTAlgorithm.ChannelRepresentation(objectModelUpdate, greyBlur, greyBlurSize, channelSize);

    } else {


        // grab expected motion x and y values
        double expectedXmovement;
        double expectedYmovement;
        expectedMotion.getAverageMovement(expectedXmovement, expectedYmovement);

        //Calculate the expected location of the object. Ensure that the rectangle at this
        //location is contained entirely in the image to avoid indexing errors
        double expectedX = boundsCheck(currentObject.getX() + expectedXmovement, currentObject.getWidth(), inputImage.ni());
        double expectedY = boundsCheck(currentObject.getY() + expectedYmovement, currentObject.getHeight(), inputImage.nj());

        // apply gradient descent search
        objectPosition = gradientDescent.findObject(channelRepresentationInput, currentObject.getX() + expectedXmovement,
                                                    currentObject.getY() + expectedYmovement, model ,
                                                    currentObject.getWidth(), currentObject.getHeight(), maxDistance);

        currentObject.setX(objectPosition[0]);
        currentObject.setY(objectPosition[1]);

        //create model from new object positions
        objectModelUpdate = vil_crop(inputImage, currentObject.getX(), currentObject.getWidth(), currentObject.getY(), currentObject.getHeight());// crops image to object top left corner

        //get the channel representation of this new model
        objectUpdate = EDFTAlgorithm.ChannelRepresentation(objectModelUpdate, greyBlur, greyBlurSize, channelSize);

        //update model by a learning factor
        model = DfAlgorithm.Update_DF(model,objectUpdate, learningFactor);

    }

    // clear the image buffers
    objectUpdate.clear();
    channelRepresentationInput.clear();

    //updated the motion
    expectedMotion.newMovement(currentObject.getX() - initialPosition.getX(), currentObject.getY() - initialPosition.getY());

    return currentObject;
}

int EDFTTracker::boundsCheck(int expectedLocation, int objectDimension, int imageDimension){
    return std::min(std::max(0, expectedLocation), imageDimension - objectDimension);
}


