#ifndef RECTANGLE_H
#define RECTANGLE_H


class Rectangle
{
    public:
        Rectangle();
		Rectangle(double x, double y, double width, double height);
        ~Rectangle();
        double getX() { return x; }
        double getY() { return y; }
        double getWidth() { return width; }
        double getHeight() { return height; }
        double getEndPositionX(){return (x + width);}
        double getEndPositionY(){return (y + height);}
        void setX(double X);
        void setY(double Y);
        void setWidth(double Width);
        void setHeight(double Height);
    protected:
    private:
        double x;
        double y;
        double width;
        double height;
};

#endif // RECTANGLE_H
