#include "include\InputText.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

using namespace std;

InputText::InputText()
{
    //ctor
}

InputText::~InputText()
{
    //dtor
}


//const char * FileDirectory
std::map<string, InputTextArgument> InputText::getText() {

    std::map<string, InputTextArgument> inputArgMap;
    const char* FileDirectory = "InputFile.txt";

    int lineCount = 0;
    string line;
    ifstream inputFile(FileDirectory);

    if (inputFile.is_open()) {
        while (getline(inputFile, line)) {

            cout << line << endl;

            //Split the line by whitespace
            std::istringstream stream(line);
            std::vector<std::string> split;

            for(int i = 0; i < 2; i++){
                string text;
                stream >> text;
                split.push_back(text);
            }

            if (split.size() >= 2){
                //Add to the input arguments, using the first value as the key and the second one as the value
                inputArgMap[split[0]] = InputTextArgument(split[1]);
            } else {
                cout << "Unable to parse line in input file" << endl;
            }
        }
    } else {
        cout << "Input file was unable to be read..." << endl;
    }

    return inputArgMap;
}
