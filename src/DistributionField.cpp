#include "include\DistributionField.h"
#include <vil/vil_image_view.h>
#include <vcl_vector.h>
#include <vil/algo/vil_gauss_filter.h>
#include <math.h>
#include <stdlib.h>
#include <vil/vil_save.h>


DistributionField::DistributionField()
{
    //ctor
}

DistributionField::~DistributionField()
{
    //dtor
}




vcl_vector<vil_image_view<double> >  DistributionField::Create_DF( vil_image_view<double>  inputImage ,double greyBlur, double colourblur , double greyBlurSize, double colourBlurSize,int Num_channels){


    int Channel_width = ceil((256 + 0.0f) / Num_channels);  //0.0f helps it round up. This needs to round up otherwise floor() below over flows vector

    unsigned int Channel;

    //double half_width_spatial = Spatial_blur_size * 3;
    //double half_width_colour = 0.6;




    //allocate memory to the image
    vil_image_view<double> OneDimensionalFilter;//(1, Num_channels, inputImage.nplanes());
    OneDimensionalFilter.set_size( 1, Num_channels, inputImage.nplanes());

    vcl_vector<vil_image_view<double> > DF;


   for (unsigned p=0;p < inputImage.nplanes();++p){
        for (unsigned j=0;j < Num_channels;++j){
            for (unsigned i=0;i < 1;++i){

                OneDimensionalFilter(i,j,p) = (double)(i+j+p);

            }
        }
    }

    for (int c = 0; c < Num_channels;c++){

        vil_image_view<double> InputImageBuffer(inputImage.ni(),inputImage.nj(), inputImage.nplanes());
        DF.push_back(InputImageBuffer);

        for(int x = 0; x < inputImage.ni(); x++){
            for(int y = 0; y < inputImage.nj(); y++){
                for(int i = 0; i < inputImage.nplanes(); i++){

                    DF[c]( x, y, i) = 0;

                    if(DF[c](x,y,i) != 0){vcl_cout <<" DF is incorrect " << vcl_endl;}
                }
            }
        }
    }


    // fill the DF
    // after this process, we have a set of channels that indicate exactly where in the source
    // image each colour occurs

    for(int x = 0; x < DF[0].ni(); x++){
        for(int y = 0; y < DF[0].nj(); y++){
                for(int i = 0 ; i < DF[0].nplanes();i++){
            //introduce outer loop, that loops through images
            Channel =int(floor(inputImage(x, y, i) / Channel_width)) ;    // work out which channel the current pixel corresponds to.... floor rounds value down.



                DF[Channel](x,y,i) = 1.0;

            }
        }
    }




    for(int c = 0 ; c < Num_channels; c++)
    {
     vil_gauss_filter_2d(DF[c], DF[c],greyBlur,greyBlurSize);
    }





   for(unsigned int x = 0; x < inputImage.ni()-1; x++){
       for(unsigned int y = 0; y < inputImage.nj()-1; y++){
            for (unsigned int c = 0; c < Num_channels-1; c++){

                for(unsigned int i = 0; i < inputImage.nplanes(); i++){

                        OneDimensionalFilter(0,c,i) = DF[c](x,y,i);
                      //  vcl_cout <<OneDimensionalFilter(c,0,0) << vcl_endl;
                    }
                }

                vil_gauss_filter_1d(OneDimensionalFilter, OneDimensionalFilter,colourblur,0.9);

                for (unsigned int c = 0; c < Num_channels-1; c++){
                    for(unsigned int i = 0; i < inputImage.nplanes(); i++){

                        DF[c](x,y,i) = OneDimensionalFilter(0,c,i);

                    }
                }
        }
    }


    return DF;  //or pass through
}

vcl_vector<vil_image_view<double> > DistributionField::Update_DF(vcl_vector<vil_image_view<double> >originalDF, vcl_vector<vil_image_view<double> > newDF, double learningRate){

    if (originalDF.size() == newDF.size()){
        for (int c = 0; c  <  originalDF.size(); c++){
            if (originalDF[c].nplanes() == newDF[c].nplanes()){
                for (int x = 0; x < originalDF[c].ni(); x++){
                    for (int y = 0; y < originalDF[c].nj(); y++){
                        for (int i = 0; i < originalDF[c].nplanes(); i++){

                            originalDF[c]( x, y, i) =(originalDF[c]( x, y, i)*(1 - learningRate)) + (newDF[c]( x, y, i) * learningRate);

                        }
                    }
                }

            }else{
                vcl_cout << "nplanes error"<<vcl_endl;
                return originalDF;
            }

        }

        return originalDF;

    }else{
        //print error for size of DF images
        vcl_cout << "size error"  <<vcl_endl;
        return originalDF;
    }

}



double DistributionField::Compare_DF( vcl_vector<vil_image_view<double> > originalDF, vcl_vector<vil_image_view<double> > newDF ){

double distance = 0;       // distance between both DF's also an output of this function

    if (originalDF.size() == newDF.size()){

        for (int c = 0; c  <  originalDF.size(); c++){
            if (originalDF[c].nplanes() == newDF[c].nplanes()){

                for (int x = 0; x < originalDF[c].ni(); x++){
                    for (int y = 0; y < originalDF[c].nj(); y++){
                        for (int i = 0; i < originalDF[c].nplanes(); i++){

                          //  originalDFGreyValue += originalDF[c](x,y,i);
                           // newDFGreyValue += newDF[c](x,y,i);
                             distance = distance + fabs(newDF[c](x,y,i) - originalDF[c](x,y,i));

                        }

                    }
                }

            }else{
                vcl_cout << "nplanes error"<<vcl_endl;
                return 0;
            }

        }

        return distance;

    }else{
        //print error for size of DF images
        vcl_cout << "size error"  <<vcl_endl;
        return 0;
    }

}
