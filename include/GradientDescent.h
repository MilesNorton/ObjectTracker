#ifndef GRADIENTDESCENT_H
#define GRADIENTDESCENT_H

#include <vcl_vector.h>
#include <vil/vil_image_view.h>
class GradientDescent
{
    public:
        GradientDescent();
        ~GradientDescent();
        vcl_vector <double> findObject(vcl_vector<vil_image_view<double> >Model,double initialX,double initialY,vcl_vector<vil_image_view <double> >  objectModel,double objectWidth, double objectHeight, int maxDistance);


    protected:
    private:
};

#endif // GRADIENTDESCENT_H
