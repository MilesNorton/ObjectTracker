#include "include\GradientDescent.h"
#include "include\DistributionField.h"
#include <vil/vil_crop.h>
#include <math.h>
#include <stdlib.h>



#include <vil/algo/vil_gauss_filter.h>  // 2d filter for better accuracy
#include <vcl_iostream.h>
#include <iostream>
#include <sstream>
#include <vcl_iostream.h>
#include <vcl_sstream.h>
#include <iomanip>      // allows set precision member for double types
#include <fstream>      //creates output file .txt
using namespace std;  // needed for output txt file (ofstream)






GradientDescent::GradientDescent()
{
    //ctor
}

GradientDescent::~GradientDescent()
{
    //dtor
}

vcl_vector <double> GradientDescent::findObject(vcl_vector <vil_image_view<double> > Model, double initialX, double initialY, vcl_vector <vil_image_view<double> > objectModel, double objectWidth, double objectHeight,int maxDistance){

    double newXposition = initialX;
    double newYposition = initialY;
    int Num_Search_Locations = 5;
    int Min_distance; // a big number
    int Best_location;
    double distance;
    double width =  objectModel[0].ni();
    double height =  objectModel[0].nj();
    DistributionField distributionField;

    vcl_vector <double> finalXY;
    vcl_vector <signed int> searchLocationsY;
    vcl_vector <signed int> searchLocationsX;

    searchLocationsX.push_back(0);   //Might need to use pushback
    searchLocationsX.push_back(-1);
    searchLocationsX.push_back(0);
    searchLocationsX.push_back(1);
    searchLocationsX.push_back(0);
    searchLocationsY.push_back(0);
    searchLocationsY.push_back(0);
    searchLocationsY.push_back(-1);
    searchLocationsY.push_back(0);
    searchLocationsY.push_back(1);

    vcl_vector<vil_image_view<double> > croppedModel;

    vil_image_view <double> objectModelUpdate;

    int cropWidth;
    int cropHeight;

    do {

        Min_distance = 99999999; // a big number
        Best_location = 0;

        for (int i = 0; i < Num_Search_Locations; i++){

            cropWidth = newXposition + searchLocationsX[i];
            cropHeight = newYposition + searchLocationsY[i];

            // below statements are a quick fix to the boundary conditions, this shouldn't even be zero but for some reason the tracking box is going of screen
            if (cropWidth < 0){cropWidth = 0;}
            if (cropHeight < 0){cropHeight = 0;}

            for (int c = 0; c < objectModel.size(); c++){

                objectModelUpdate = vil_crop(Model[c],cropWidth,objectWidth, cropHeight,objectHeight );
                croppedModel.push_back(objectModelUpdate);// crops image to object top left corner.

            }

            distance = distributionField.Compare_DF(objectModel, croppedModel);
            croppedModel.clear();

            if(distance < Min_distance){

                Min_distance = distance;

                Best_location = i;
            }

        }

            newXposition = newXposition + searchLocationsX[Best_location];
            newYposition = newYposition + searchLocationsY[Best_location];


    }while ((Best_location != 0)  && (sqrt(pow(newXposition - initialX,2) + pow(newYposition - initialY,2)) < maxDistance));

        finalXY.push_back(newXposition);  //new starting x position
        finalXY.push_back(newYposition);  //new starting y position
        return finalXY;// the initial values are sent back through the function &&
}
