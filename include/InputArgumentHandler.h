#ifndef INPUTARGUMENTHANDLER_H
#define INPUTARGUMENTHANDLER_H

#include "include\InputArguments.h"
#include "include\InputText.h"

#include <map>


class InputArgumentHandler
{
    public:

        InputArgumentHandler();
        ~InputArgumentHandler();

        //grabs either a txt file input or CMD input and returns inputs
        InputArguments getArguments(int argc,  char * argv[]);

    protected:

        //This function checks to see if number is 0. If it is, return default else will
        //return number
        double compareWithDefault(double number, double defaultVal);
        unsigned int compareWithDefault(unsigned int number, unsigned int defaultVal);
        string compareWithDefault(string str, string defaultVal);

    private:
};

#endif // INPUTARGUMENTHANDLER_H

