#include "include\InputTextArgument.h"

InputTextArgument::InputTextArgument(){
}

InputTextArgument::~InputTextArgument()
{
    //dtor
}

std::string InputTextArgument::getAsString(){
    return argument;
}

unsigned int InputTextArgument::getAsUInt(){
    std::istringstream reader(argument);
    unsigned int value;
    reader >> value;
    return value;
}

double InputTextArgument::getAsDouble(){
    return atof(argument.c_str());
}


