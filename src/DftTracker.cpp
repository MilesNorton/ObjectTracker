#include "include\DftTracker.h"
#include <vil/vil_crop.h>
#include "include\GradientDescent.h"
#include "include\Rectangle.h"
#include <vcl_vector.h>
#include <math.h>

#include <vil/vil_save.h>
#include <vcl_iostream.h>
#include <iostream>



#include <vil/algo/vil_gauss_filter.h>  // 2d filter for better accuracy
#include <vcl_iostream.h>
#include <iostream>
#include <sstream>
#include <vcl_iostream.h>
#include <vcl_sstream.h>
#include <iomanip>      // allows set precision member for double types
#include <fstream>      //creates output file .txt
using namespace std;  // needed for output txt file (ofstream)


DftTracker::DftTracker()
{
    //ctor
}

DftTracker::~DftTracker()
{
    //dtor
}

Rectangle DftTracker::track(vil_image_view<double> inputImage, int i){

    DistributionField DfAlgorithm;
    GradientDescent gradientDescent;
    Rectangle rectangle;

    vcl_vector<double> objectPosition;

    vcl_vector<vil_image_view<double> > Model;

    vil_image_view <double> objectModelUpdate;
    vil_image_view <double> objectModelUpdateTest;
    vcl_vector<vil_image_view<double> > objectUpdate;



    //initialise model and object model images here. type image view
    objectPosition.push_back(initialObject.getX());
    objectPosition.push_back(initialObject.getY());
  //  objectPosition.push_back(0);    // added width
   // objectPosition.push_back(0);    // added height

//int count= 0;
vcl_stringstream outputName;

  //  for (int i = 0; i < inputImages.size(); i++){



        Model = DfAlgorithm.Create_DF( inputImage, greyBlur, colourBlur, greyBlurSize, colourBlurSize, channelSize);


        if (i == 0){ //first frame

            currentObject = initialObject;

            objectModelUpdate = vil_crop(inputImage, currentObject.getX(), currentObject.getWidth(), currentObject.getY(), currentObject.getHeight());// crops image to object top left corner.

            model = DfAlgorithm.Create_DF(objectModelUpdate, greyBlur, colourBlur, greyBlurSize, colourBlurSize, channelSize);

        } else{

            objectPosition = gradientDescent.findObject(Model, currentObject.getX(), currentObject.getY() , model , currentObject.getWidth(), currentObject.getHeight(), maxDistance); // pass back the initial x and y as the new initial x and y for the next image

            currentObject.setX(objectPosition[0]);
            currentObject.setY(objectPosition[1]);

            objectModelUpdate = vil_crop(inputImage, currentObject.getX() ,currentObject.getWidth() , currentObject.getY() ,currentObject.getHeight());// crops image to object top left corner

            objectUpdate = DfAlgorithm.Create_DF( objectModelUpdate, greyBlur, colourBlur, greyBlurSize, colourBlurSize, channelSize);

            model = DfAlgorithm.Update_DF(model,objectUpdate, learningFactor);

        }

            objectUpdate.clear();
            Model.clear();

         //   rectangle.setX(objectPosition[0]);
         //   rectangle.setY(objectPosition[1]);
         //   rectangle.setWidth(initialObject.getWidth());
         //   rectangle.setHeight(initialObject.getHeight());



         //   rectangleVector.push_back(rectangle);



  //  }

    return currentObject;
}
