#include "include\EnhancedDistributionField.h"
#include <vil/algo/vil_gauss_filter.h>
EnhancedDistributionField::EnhancedDistributionField()
{
    //ctor
}

EnhancedDistributionField::~EnhancedDistributionField()
{
    //dtor
}


vcl_vector<vil_image_view<double> > EnhancedDistributionField::ChannelRepresentation(vil_image_view<double>  inputImage,double greyBlur, double greyBlurSize, int Num_channels )
{
    // determine bin width and the bin centres
    double binWidth = 256.0/((double)Num_channels);
    double* binCentres = new double[Num_channels];

    binCentres[0] = binWidth/2.0;

    for (int i = 1; i < Num_channels; i++){

        binCentres[i] = binCentres[i - 1] + binWidth;

    }

    // pre-compute a look-up table for speedMotion ExpectedMotion
    // allocate it
    double** LookUpTable = new double*[256];

    for (int i = 0; i < 256; i++){

        LookUpTable[i] = new double[Num_channels];
    }

    // fill the LookUpTable
    for (int i = 0; i < 256; i++)
    {
        for (int j = 0; j < Num_channels; j++)
        {
            double binDist = fabs(binCentres[j] - ((double)i));

            if (binDist <= (binWidth/2.0)){
                LookUpTable[i][j] = 0.75 - pow((binDist/binWidth), 2.0);
                }
            else if (binDist <= (binWidth*1.5)){
                LookUpTable[i][j] = pow(((binDist/binWidth) - 1.5), 2.0)/2.0;
                }
            else{
                LookUpTable[i][j] = 0.0;
                }
        }
    }

    int Channel_width = 256 / Num_channels;
    vcl_vector<vil_image_view<double> > CR;

   for (int c = 0; c < Num_channels;c++){

        vil_image_view<double> InputImageBuffer(inputImage.ni(),inputImage.nj(), inputImage.nplanes());
        CR.push_back(InputImageBuffer);

        for(int x = 0; x < inputImage.ni(); x++){
            for(int y = 0; y < inputImage.nj(); y++){
                for(int i = 0; i < inputImage.nplanes(); i++){

                    CR[c]( x, y, i) = 0;

                    if(CR[c](x,y,i) != 0){vcl_cout <<" DF is incorrect " << vcl_endl;}
                }
            }
        }
    }




    // after this process, we have a set of channels that indicate exactly where in the source
    // image each colour occurs
    for(int x = 0; x < inputImage.ni(); x++){
        for(int y = 0; y < inputImage.nj(); y++){
            for (int c = 0; c < Num_channels;c++){
                for(int i = 0; i < inputImage.nplanes(); i++){

                    CR[c](x,y,i) = LookUpTable[ int (inputImage(x, y, i ))][c];     //input image needs to be of type int
                }
            }
        }
    }

     //2d gauss filtering
    for(int c = 0 ; c < Num_channels; c++)
    {
     vil_gauss_filter_2d(CR[c], CR[c],greyBlur,greyBlurSize);
    }
    return CR;
}
