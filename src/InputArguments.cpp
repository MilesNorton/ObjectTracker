#include "include\InputArguments.h"

InputArguments::InputArguments(){}

InputArguments::~InputArguments(){}

void InputArguments::setInitialX(double startingX){
    initialX = startingX;
}

void InputArguments::setInitialY(double startingY){
    initialY = startingY;
}

void InputArguments::setInitialWidth(double startingWidth){
    initialWidth = startingWidth;
}

void InputArguments::setInitialHeight(double startingHeight){
    initialHeight = startingHeight;
}

void InputArguments::setMaxSearchDistance(unsigned int searchDistance){
    maxSearchDistance = searchDistance;
}

void InputArguments::setAlgorithmType(vcl_string algorithm){
    algorithmType = algorithm;
}

void InputArguments::setInputPath(vcl_string input){
    inputPath = input;
}

void InputArguments::setOutputPath(vcl_string output){
    outputPath = output;
}

void InputArguments::setFileExtension(vcl_string extension){
    fileExtension = extension;
}

void InputArguments::setcolourBlur(double CBlur){
    colourBlur = CBlur;
}

void InputArguments::setgreyBlur(double GBlur){
    greyBlur = GBlur;
}

void InputArguments::setColourBlurSize(double CBlurSize){
    colourBlurSize = CBlurSize;
}

void InputArguments::setGreyBlurSize(double GBlurSize){
    greyBlurSize = GBlurSize;
}


void InputArguments::setChannelSize(int ChannelSize){
    channelSize = ChannelSize;
}

void InputArguments::setColourInput(vcl_string InputType){
    colourInput = InputType;
}

void InputArguments::setLearningFactor(double LearningFactor){
    learningFactor = LearningFactor;
}
