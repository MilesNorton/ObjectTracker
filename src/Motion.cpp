#include "include\Motion.h"

Motion::Motion()
{
    // initial motion is zero
    averageXmovement = 0;
    averageYmovement = 0;
    framesProcessed = 0;
}

Motion::~Motion()
{
    //dtor
}


void Motion::newMovement(double x, double y){
    framesProcessed++;
    averageXmovement = rollingAverage(averageXmovement, x);
    averageYmovement = rollingAverage(averageYmovement, y);
}

double Motion::rollingAverage(double currentAverage, double newSample){
    int number = std::min(AVERAGE_NUMBER, framesProcessed);
    currentAverage -= currentAverage / number;
    currentAverage += newSample / number;

    return currentAverage;
}

void Motion::getAverageMovement(double& xAvg, double& yAvg){
    xAvg = averageXmovement;
    yAvg = averageYmovement;
}
