#ifndef DISTRIBUTIONFIELD_H
#define DISTRIBUTIONFIELD_H

#include <vcl_vector.h>
#include <vil/vil_image_view.h>

class DistributionField
{
    public:
        DistributionField();
        ~DistributionField();

        vcl_vector<vil_image_view<double> > Create_DF( vil_image_view<double> inputImage ,double Spatial_blur_size, double Colour_blur_size, double greyBlurSize, double colourBlurSize, int Num_channels);
        vcl_vector<vil_image_view<double> > Update_DF(vcl_vector<vil_image_view<double> > originalDF, vcl_vector<vil_image_view<double> >  newDF, double learningFactor);
        double Compare_DF( vcl_vector<vil_image_view<double> > originalDF, vcl_vector<vil_image_view<double> > newDF);

    protected:
    private:
};

#endif // DISTRIBUTIONFIELD_H
