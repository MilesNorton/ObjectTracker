#ifndef TRACKER_H
#define TRACKER_H

#include "include\Rectangle.h"
#include "include\InputArguments.h"
#include "include\Motion.h"
#include <vcl_vector.h>
#include <vil/vil_image_view.h>

class Tracker
{
    public:

        Tracker();
        ~Tracker();
        void initialise(InputArguments inputArguments);

        virtual Rectangle track(vil_image_view<double> inputImages, int i) = 0;

    protected:

        //Initial position of the tracked object
        Rectangle initialObject;

        //Current position of the tracked object
        Rectangle currentObject;

        double learningFactor;
        int maxDistance;
        double greyBlur;
        double colourBlur;
        double greyBlurSize;
        double colourBlurSize;
        int channelSize;

        vcl_vector<vil_image_view <double> > model;
    private:
};

#endif // TRACKER_H
