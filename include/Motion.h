#ifndef MOTION_H
#define MOTION_H

#include <algorithm>


const int AVERAGE_NUMBER = 5;

class Motion
{
    public:
        Motion();
        ~Motion();

        void newMovement(double x, double y);
        void getAverageMovement(double& xAvg, double& yAvg);

    protected:
        //Calculates the rolling average from the current average and the new sample, over
        //the last averagedOver values
        double rollingAverage(double currentAverage, double newSample);
    private:
        double averageXmovement;
        double averageYmovement;
        int framesProcessed;
};

#endif // MOTION_H
